import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._

val sc = new SparkContext("local", "Search Text", new SparkConf())

val textFile = sc.textFile("searchtext.txt")
val df = textFile.toDF("line")
val errors = df.filter(col("line").like("%Canada%"))

println(s"Number of students from Canada =>")
errors.count()
println(s"Number of MALE students from Canada =>")
errors.filter(col("line").like("%Male%")).count()
println(s"Details of MALE students from Canada =>")
errors.filter(col("line").like("%Male%")).collect()

sc.stop()
System.exit(0)